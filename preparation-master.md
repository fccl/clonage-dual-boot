# Préambule

Ce guide s’adresse à ceux souhaitant cloner sur un parc informatique, une image contenant deux systèmes d’exploitations. Vu la difficulté à réaliser cela, posez-vous d’abord la question existentielle qui est : ai-je besoin de Microsoft Windows ? En effet, un clonage avec uniquement un système GNU/Linux est beaucoup plus simple à effectuer, car ne nécessite pas au préalable des technologies imposées de « sécurité » (selon Microsoft) tels que Secure Boot, ajoutant une multitude d’étapes pour parvenir à un résultat similaire.

## Activer Secure Boot et TPM2

> Windows 11 nécessite officiellement la présence des technologies de « sécurité » Secure Boot et TPM2 sur la carte mère de l’ordinateur. La présence de ces technologies requière par la même occasion le mode UEFI.
> Il est nécessaire de bien veiller a ce que ces technologies soient activées lors de l’installation des systèmes d’exploitation.

Se rapporter à la doc relative à l'UEFI de l’ordinateur.

## Logique d’installation

> On installe dans un premier temps Microsoft Windows, puis GNU/Linux. Ce dernier à l’avantage de reconnaître l'ensemble des systèmes de fichiers, n’invitant pas à supprimer par « maladresse » les espaces allouées aux autres systèmes.

# Microsoft Windows 11

## Création image Windows

> Utiliser le logiciel graphique officiel de Microsoft pour la création de la clef USB d’installation, ne semble plus supporter les solutions alternatives qui génèrent des erreurs. Celui-ci, tout comme l’image du système, est disponible sur le site officiel de Microsoft.

## Installation Windows

> Ne tenez pas compte pour le moment de l’espace qui sera alloué à GNU/Linux : Recouvrez l’ensemble du disque.

### Compte Microsoft

> Lors de l’installation, via le compte utilitaire individuel basique, est demandé de lier le système à un compte Microsoft. Pour éviter de s’« emmerder » avec cela, voici la procédure à suivre :

- Mettre une adresse de courriel inexistante (exemple **0@0.com**), un mot de passe quelconque (exemple **0**).
- Après avoir constaté que le compte n’existe pas, le système invitera à créer un compte local.

### Questions-réponses

Demande trois questions réponses : Possible de mettre la même réponse pour les trois questions.

## Configurations Windows

### Création d’un compte invité

> Création d’un compte utilisateur lambda que l’on va configurer, puis passage dans le groupe invité pour réduire les possibilités de personnalisation de l’environnement

Via Powershell en admin, on crée le compte utilisateur

`net user Invité /add`

Supprimer le compte du groupe Users

`net localgroup Users Invité /delete`

Ajouter le compte dans le groupe Guests

`net localgroup Guests Invité /add`

> À noter que vous devrez le remettre dans le groupe Users pour avoir la possibilité de configurer la session à nouveau.

## Chocolatay

Chocolatay est un outil permettant de gérer les logiciels sur Windows comme ce qui se fait sur GNU/Linux : via un gestionnaire de paquet. Ce qui rend la gestion de parc informatique beaucoup plus puissant et simple à gérer.
Avant d’installer un quelconque logiciel, installez déjà cet outil au préalable.

### Installer Chocolatay

Passez la commande dans Powershell (avec les droits d’administration), disponible sur le site web officiel :
https://chocolatey.org/install#individual

> À noter que si vous gérez un parc informatique très conséquent, il faudra vous poser la question d’héberger votre propre serveur Chocolatay ou passer par un service commercial.

### Installer les logiciels

> Les logiciels s’installent en ligne de commande dans Powershell.

Exemple, pour installer Firefox ESR, VLC et LibreOffice Still :

`choco install firefoxesr vlc libreoffice-still -y`

Il existe aussi une interface graphique permettant de gérer les logiciels via Chocolatey :

`choco install ChocolateyGUI -y`

Globalement, vous pouvez trouver, sélectionner et générer un script via cette page web : https://community.chocolatey.org/packages/

## Nettoyage

### Utilitaires

Ne pas hésiter à supprimer les programmes jugés inutiles. Désactiver les widgets présents sur le bureau, etc.

Attention : ces configurations ont un impact uniquement sur la session courante.

## Processus

Passez dans chaque logiciel installé via Chocolatey pour supprimer les gestionnaires de mises à jour. Cela économisera un peu de ressources et limitera les notifications intempestives (ainsi seul le processus Chocolatey gère les mises à jours).

Attention : ces configurations ont un impact uniquement sur la session courante.

### Publicités

Dans le « nettoyage », penser à supprimer les pubs dans l’écran de démarrage : https://www.justgeek.fr/windows-11-desactiver-publicites-astuces-ecran-de-verrouillage-90398/

- Raccourci clavier **Windows** (alias Super) + **i** ,
- `Personnalisation`, `Écran de verrouillage`,
- `Personnaliser votre écran de verrouillage` = **Image**,
- Décocher `Personnaliser l’écran de verrouillage, notamment avec des anecdotes et des astuces`.

## Processus

### Bitlocker

> Microsoft intègre désormais la technologie de chiffrement « Bitlocker ». Il faut la désactiver pour permettre à Fog de pouvoir capturer le système.

Dans Powershell en tant qu’administrateur, écrire

`manage-bde off c:`

### Démarrage rapide

Désactiver le démarrage rapide de Microsoft, sans quoi il ne sera pas possible de capturer le système.

`Parametre systeme`, `Option d’alimentation`, Options avancés`.

### Bleachbit

Passer un coup de Bleachbit pour supprimer les fichiers temporaires et inutiles, cela réduira la taille de la capture.

### Défragmentation

Passez par l’outil de défragmentation de Windows avant de passer à la suite.

# GNU/Linux

> Est utilisé Debian 12 Cinnamon, celui-ci utilise le gestionnaire d’affichage Lightdm.

## Partitionnement

Démarrer sur une clef live USB contenant le logiciel de partition efficace Gparted (https://gparted.org/download.php). Debian Cinnamon a un utilitaire trop basique.

Avec Gparted, conservez les partitions de Microsoft Windows, mais réduisez la partition principale en taille, permettant d’installer GN/Linux à côté.

> Microsoft Windows nécessite au strict minimum 50 go de disponibilité sur le disque pour gérer notamment les mises à jour. Si vous souhaitez l’employer régulièrement pour différents usages, gardez à minima 100 go. Concernant GNU/Linux, à moins que vous souhaitiez utiliser des outils spécifiques, globalement il a besoin d’environ 3 fois moins de place sur le disque pour usage équivalent.

## Installation

> Rappel : Secure boot et TPM2 doivent être activés avant l’installation de GNU/Linux.

Indiquer la première partition (Fat32 -100 mo) en point de montage **/boot/efi**

Vous pouvez utiliser une unique partition pour GNU/Linux, qui gère à la fois les logiciels, fichiers et swap.

## Paquets

> Contrairement à Windows, la désinstallation des utilitaires disponibles nativement est valable pour l’ensemble des utilisateurs.

Ouvrez un terminal pour les commandes suivantes.

### Installer logiciels

Exemple pour installer ces logiciels (mono est nécessaire pour installer le client Fog, numlockx pour activer automatiquement le pavé numérique).

<code>apt install apt-xapian-index bleachbit mono-complete numlockx -y</code>

### Désinstaller logiciels

> Exemple sur Debian 12 Cinnamon (beaucoup de jeux)

<code>apt --purge autoremove gnome-2048 five-or-more hitori gnome-chess gnome-klotski gnome-mines gnome-nibbles quadrapassel four-in-a-row gnome-robots aisleriot gnome-sudoku swell-foop tali gnome-taquin gnome-tetravex gnome-mahjongg lightsoff iagno deja-dup goldendict hexchat pidgin remmina transmission-common totem sound-juicer rhythmbox brasero gnome-sound-recorder cheese gnote xiterm+thai mlterm mlterm-common xterm

## Personnalisation

### Le web

> Pour améliorer l’expérience utilisateur sur le web, installer un bloqueur de scripts sur le navigateur Firefox. Attention ce paramétrage n’est valable que sur la session courante.

Configuration du navigateur web :
- Ajouter Ublock Origin
-> Configurer selon la procédure disponible sur https://bloquelapub.net/
-> Ajouter le dépôt « I don't care about cookies » https://www.i-dont-care-about-cookies.eu/abp/

### Traitement Grammaticale

> Attention ce paramétrage n’est valable que sur la session courante.

Ajouter Grammalecte à LibreOffice : https://grammalecte.net/#download

## Configuration système

### Hostname

> Il est possible que le nom du système possède une référence au modèle d’ordinateur. Il est possible d’attribuer à GNU/Linux un nom d’hôte générique (qui apparaîtra sur les clones).

hostnamectl set-hostname nouveau_nom_generique

### Lightdm

**Clavier numérique**

Ajouter cette ligne dans le fichier **/etc/lightdm/lightdm.conf**

`greeter-setup-script=/usr/bin/numlockx on`

**Afficher noms d’utilisateurs**

Décommenter dans le fichier **/etc/lightdm/lightdm.conf**

`greeter-hide-users=false`

**Temps de sélection Grub**

> Exemple pour laisser plus de temps aux usagers de choisir Windows au démarrage de la machine

Modifier valeur `GRUB_TIMEOUT=*` dans fichier /etc/default/grub. Reconfigurer Grub avec `update-grub`

## Compte invité

> Il semble que la fonctionnalité via Lightdm n’est plus fonctionnelle. Une solution alternative est proposée.

### Compte guest-prefs

> Ce compte sera utilisé pour pré-configurer la session invitée. Ses fichiers seront chargés depuis le compte invité dans le dossier /etc/skel

Créer un utilisateur **guest-prefs**

`adduser guest-prefs`

Créer un lien symbolique vers **/etc/skel/**

`ln -s /home/guest-prefs /etc/skel`

### Compte guest

Créer un utilisateur **guest**

`adduser guest`

Supprimer le mot de passe

`passwd -d guest`

Créer un script dans le fichier **/usr/local/bin/guest-user.sh**

<code>#!/usr/bin/env bash
set -e\
shopt -s dotglob
rm -rf /home/guest
install -d -m755 /home/guest
cp -a /etc/skel/guest-prefs/* /home/guest
chown -R guest:guest /home/guest/*</code>

Donner les droits d’exécution à ce script

`chmod +x /usr/local/bin/guest-user.sh`

Créer un lanceur pour exécuter le script au démarrage, le fichier sera **/etc/systemd/system/guest-user.service**

<code>[Unit]
Description=Setup guest account
After=syslog.target\
[Service]
Type=oneshot
ExecStart=/usr/local/bin/guest-user.sh\
[Install]
WantedBy=default.target</code>

Dans le répertoire **/etc/systemd/system**

`systemctl enable guest-user`

### Tester le script

Prendre en compte le script systemctl

`systemctl daemon-reload`

verifier si le script ne fait pas d'erreur apres redemeragge 

`systemctl status guest-user`

## Client Fog

Téléchargez le client Fog, rendez-le exécutable. Ne l’exécutez que lorsque le clone sera déployé (sans quoi Fog ne saura pas différentier les clients).

## Optimisation

Passez un coup de Bleachbit pour supprimer les paquets des dépôts et divers fichiers temporaires et inutiles. Cela réduira la taille de l’image à capturer.

# Après clonage

## Boot et Grub

> Démarrer dans un premier temps les systèmes sans Secure boot et TPM2 pour éviter les blocages liés aux systèmes de sécurités.

> Régulièrement, l'UEFI est perdu et choisi de snober GNU/Linux au profit de Microsoft Windows, donc uniquement Windows peut démarrer. Il faut « réparer » Grub.

### Réparer Grub

**Pas de GNU/Linux**

> Il existe divers façons de faire, le plus simple est de passer par **boot-repair-disk** (https://sourceforge.net/projects/boot-repair-cd/files/), un système en live-USB qui propose une option **Réparation recommandée** au démarrage. Un clic et l’outil gère l’ensemble de la configuration automatiquement. Ensuite vous pouvez redémarrer.

**Pas de Microsoft Windows**

> Nouveau problème, seul GNU/Linux est proposé au démarrage… à priori c’est une nouvelle règle de sécurité introduite dans GNU/Linux.

Sur Debian 12, ouvrez le fichier **/etc/default/grub**, Déommentez 

`GRUB-DISABLE-OS-PROBER=false`

Puis relancez la configuration de Grub

`update-grub`

## Hostname

> Si vous souhaitez attribuer un nom d’hôte pour chaque machine

**GNU/Linux**

`hostnamectl set-hostname nouveau_nom_individuel`

**Microsoft Windows**

> À compléter

Installer le client Fog

**GNU/Linux**

<code>cd ~/Téléchargements\
./fog-client.exe</code>

Attribuer l'**adresse IP** (ou le nom de domaine) du serveur Fog qui doit être joignable, puis validez plusieurs fois.

**Microsoft Windows**

> À compléter : À priori NetFramwork utilise une version obsolète de TLS, ce qui est rejeté par le serveur Fog pour des raisons de sécurités. À voir ce qui est possible de faire pour régler ce souci.

# Réactiver les sécurités

Redémarrez la machine, réactivez Secure Boot et TPM2.
